var express = require("express"),
    app     = express(),
    http    = require("http"),
    fs = require("fs"),
    server  = http.createServer(app);

app.configure(function () {
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
});

app.get('/', function(req, res) {
	var auto = {
		marca:"fiat",
		modelo:"punto",
		motor:"1.4"
	}

	res.send(auto);
});

app.get('/get', function (req, res) {
   fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
       console.log(data);
       res.end(data);
   });
})


server.listen(3000, function() {
	console.log("Node server running on http://localhost:3000");
});