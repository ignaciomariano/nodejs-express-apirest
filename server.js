var http = require('http');

function iniciar() {

    http.createServer(function(request, response) {
        //console.log("Petición Recibida.");
        response.writeHead(200, {"Content-Type": "text/html"});
        //response.write("Hola Mundo");
        response.end("Hola Mundo");
    }).listen(8081);
    console.log("Servidor Iniciado.");
}

exports.iniciar = iniciar;


/*
 http.createServer(function(request, response) {
 response.writeHead(200, {'Content-Type': 'text/plain'});
 response.end('Hola Mundo!');
 }).listen(8081);
 
 console.log('Servidor ejecutándose en http://127.0.0.1:8081/');
 */